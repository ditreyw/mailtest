package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"mailtest/rateLimitManager"
	"os"
	"strings"
	"sync"
)

func checkThatItsPipe() error {
	fi, err := os.Stdin.Stat()
	if err != nil {
		return err
	}

	if fi.Mode() & os.ModeNamedPipe == 0 || fi.Mode()&os.ModeCharDevice == os.ModeCharDevice || fi.Size() <= 0 {
		return errors.New("no pipe :(")
	}

	return nil
}

func getFlags() (*int, *int, *bool) {
	ratePtr := flag.Int("rate", 1, "number")
	inflightPtr := flag.Int("inflight", 1, "")
	debugPtr := flag.Bool("debug", false, "a bool")
	return ratePtr, inflightPtr, debugPtr
}

func getCmdArgs() (string, []string){
	flag.Parse()
	args := flag.Args()
	cmdName := args[0]
	cmdArgs := args[1:]
	return cmdName, cmdArgs
}

func mainErrHandler(err error) {
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}

func main() {
	mainErrHandler(checkThatItsPipe())

	ratePtr, inflightPtr, debugPtr := getFlags()
	cmdName, cmdArgs := getCmdArgs()

	rlM, err := rateLimitManager.NewRateLimitManager(
		"mainRLM",
		*ratePtr,
		*inflightPtr,
		cmdName,
		cmdArgs,
		"{}",
	)
	mainErrHandler(err)

	reader := bufio.NewReader(os.Stdin)

	var wg sync.WaitGroup

	for {
		err := rlM.CheckErr()
		mainErrHandler(err)
		str, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			} else {
				mainErrHandler(err)
			}
		}
		str = strings.TrimSpace(str)
		resC, err := rlM.ExecCmdAsyncWithRL(str)
		mainErrHandler(err)
		wg.Add(1)
		go func(resC rateLimitManager.RespChannel) {
			err := rlM.CheckErr()
			mainErrHandler(err)
			// Appropriate only if 1 response per channel
			res := <- resC
			_, err = os.Stdout.Write(res.Bytes())
			mainErrHandler(err)
			wg.Done()
		}(resC)
	}

	wg.Wait()
	if *debugPtr {
		fmt.Println("Done")
	}
}



