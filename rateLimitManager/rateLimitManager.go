package rateLimitManager

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"sync"
	"time"
)

type (
	RespChannel chan *bytes.Buffer
)

type managerBase struct {
	name string
}

func newManager(name string) *managerBase {
	return &managerBase{
		name,
	}
}

func (manager *managerBase) handleError(err error) error {
	fmt.Println(manager.name, err) // log here
	return err
}

type RateLimitManager struct {
	managerBase

	runningCmdLimitPerSec   int
	runningCmdLimitParallel int

	cmdName string
	cmdArgs []string

	placeholderVerb string
	placeholderVerbIndex int

	errC chan error
	parallelCmdGuardC chan bool
	runningCmdLimitPerSecRefreshC chan struct{}

	mut sync.Mutex

	timeLimiter chan time.Time
}

func NewRateLimitManager(
	name string,
	runningCmdLimitPerSec int,
	runningCmdLimitParallel int,
	cmdName string,
	cmdArgs []string,
	placeholderVerb string,
) (*RateLimitManager, error){
	manager := &RateLimitManager{
		managerBase:             *newManager(name),
		runningCmdLimitPerSec:   runningCmdLimitPerSec,
		runningCmdLimitParallel: runningCmdLimitParallel,
		cmdName:                 cmdName,
		cmdArgs:                 cmdArgs,
		placeholderVerb:         placeholderVerb,
		placeholderVerbIndex:    -1,
		parallelCmdGuardC:       make(chan bool, runningCmdLimitParallel),
		errC:                    make(chan error, 1),
	}

	manager.fillRunningCmdLimitC(runningCmdLimitParallel)

	if err := manager.findAndSetPlaceholderVerbIndex(); err != nil {
		return nil, err
	}

	return manager, nil
}

func (manager *RateLimitManager) fillRunningCmdLimitC(runningCmdLimit int) {
	for i := 0; i < runningCmdLimit; i++ {
		manager.parallelCmdGuardC <- true
	}
}

func (manager *RateLimitManager) findAndSetPlaceholderVerbIndex() error {
	for ind, arg := range manager.cmdArgs {
		if arg == manager.placeholderVerb {
			manager.placeholderVerbIndex = ind
			break
		}
	}

	if manager.placeholderVerbIndex == -1 {
		return errors.New("can't find placeholder verbs")
	}

	return nil
}

func (manager *RateLimitManager) formArgsForCmd(placeholderVal string) []string {
	args := make([]string, len(manager.cmdArgs))
	copy(args, manager.cmdArgs)
	args[manager.placeholderVerbIndex] = placeholderVal
	return args
}

func (manager *RateLimitManager) ExecCmdSync(arg string, cmdStdout io.Writer) (error) {
	cmd := exec.Command(manager.cmdName, manager.formArgsForCmd(arg)...)
	cmd.Stdout = cmdStdout
	err := cmd.Run()
	if err != nil {
		return err
	}
	return nil
}

func (manager *RateLimitManager) CheckErr() error {
	select {
	case err := <-manager.errC:
		return err
	default:
		return nil
	}
}

func (manager *RateLimitManager) ExecCmdAsync(resC RespChannel, arg string) {
	buf := new(bytes.Buffer)
	if err := manager.ExecCmdSync(arg, buf); err != nil {
		manager.errC <- manager.handleError(err)
	}
	resC <- buf
}

func (manager *RateLimitManager) fillTimeLimiter(t time.Time) {
	for i := 0; i < manager.runningCmdLimitPerSec; i++ {
		manager.timeLimiter <- t
	}
}

func (manager *RateLimitManager) GetTimeLimiter() <- chan time.Time {
	manager.mut.Lock()
	defer manager.mut.Unlock()
	if manager.timeLimiter == nil {
		manager.timeLimiter = make(chan time.Time, manager.runningCmdLimitPerSec)
		manager.fillTimeLimiter(time.Now())
		go func() {
			for t := range time.Tick(1 * time.Second) {
				manager.fillTimeLimiter(t)
			}
		}()
	}
	return manager.timeLimiter
}

func (manager *RateLimitManager) ExecCmdAsyncWithRL(arg string) (RespChannel, error) {
	if err := manager.CheckErr(); err != nil {
		return nil, err
	}

	resC := make(RespChannel)

	go func() {
		<- manager.GetTimeLimiter()
		<- manager.parallelCmdGuardC
		go func() {
			manager.ExecCmdAsync(resC, arg)
			close(resC)
			manager.parallelCmdGuardC <- true
		}()
	}()

	return resC, nil
}
